FBM

# 亚马逊国家简介

1：亚马逊分为欧洲、美洲、远东、中东4个大区，每个大区有多个站点。每个站点都是独立的网址。

> 黄色标记的是出单较多的站点，初始做产品时应多关注

> 站点的简称，在软件中多个地方会使用到

| 国家 | 大区 | 站点地址 | 简称 | 语言 | 货币 | 邮编 |
| ----- | ----- | ----- | ----- | ----- | ----- | ----- |
| <mark>美国</mark> | 美洲 | https://www.amazon.com | US | 英语 | 美元 | 53404 |
| <mark>加拿大</mark> | 美洲 | https://www.amazon.ca | CA | 英语 | 加元 | L6A+0K2 |
| <mark>墨西哥</mark> | 美洲 | https://www.amazon.com.mx | MX | 西班牙语 | 墨西哥元 | 28017 |
| <mark>英国</mark> | 欧洲 | https://www.amazon.co.uk | UK | 英语 | 英镑 | WR15+8SL |
| <mark>法国</mark> | 欧洲 | https://www.amazon.fr | FR | 法语 | 欧元 | 49480 |
| <mark>德国</mark> | 欧洲 | https://www.amazon.de | DE | 德语 | 欧元 | 80803 |
| <mark>意大利</mark> | 欧洲 | https://www.amazon.de | IT | 意大利语 | 欧元 | 20826 |
| <mark>西班牙</mark> | 欧洲 | https://www.amazon.es | ES | 西班牙语 | 欧元 | 18101 |
| 荷兰 | 欧洲 | https://www.amazon.nl | NL | 荷兰语 | 欧元 | 9601 BA |
| 波兰 | 欧洲 | https://www.amazon.pl | PL | 波兰语 | 欧元 | 00-675 |
| 瑞典 | 欧洲 | https://www.amazon.se | SE | 瑞典语 | 欧元 | 16446 |
| <mark>日本</mark> | 远东 | https://www.amazon.co.jp | JP | 日语 | 日元 | 121-0816 |
| <mark>澳大利亚</mark> | 远东 | https://www.amazon.com.au | AU | 英语 | 澳元 | 7109 |
| 阿拉伯联合酋长国 | 中东 | https://www.amazon.ae | AE | 英语 | 阿联酋迪拉姆 | 56617 |
| 沙特阿拉伯 | 中东 | https://www.amazon.sa | SA | 沙特语 | 沙特里亚尔 |  |



# 亚马逊名词介绍

| 名词 | 介绍 |
| ----- |:----- |
| 卖家 | 咱们 |
| 买家/客户 | 亚马逊客户 |
| 国内卖家 | 1688/淘宝等卖家 |
| 货代 | 运送国际包裹到国外买家手中的服务商，常见的有：三态、云途 |
| SKU | 卖家自己定义的商品的编码，软件会自动生成，格式是：年月日+编号 |
| ASIN | 亚马逊为每个商品分配的唯一编号 |
| UPC/EAN | 全球唯一贸易代码，理论上需要购买，实际中使用软件生成 |
| 主商品/父商品+子商品/变体 | 每个商品由一个主商品+多个子商品组成。每个子商品都称之为变体。每个变体定义了具体的颜色/尺寸。 |

# 亚马逊商品文字内容介绍

| 名词 | 介绍 | 组成部分 |  |
| ----- | ----- | ----- | ----- |
| listing| 亚马逊对产品详情页的统称，跨境卖家们戏称为李斯婷 |  |  |
| 标题 | 商品的名称，同一个商品主商品和变体的标题是一样的，唯一区别在于变体的变体末尾会追加变体的属性 | 品牌+产品名称(核心关键词前35个字符),(加一个逗号)功能/用途/形容词+名称同义+适用范围+变体属性/材质/重量/数量/尺寸等 | |
| 5点描述 | 商品描述的简单版本，可以写5条。 | 按照这样的顺序：<br/>1. 是商品卖点<br/>2. 商品材质/规格<br/>3. 功能/使用范围<br/>4. 使用方法 <br/>5. 服务声明 | |
| 产品说明 | 商品描述的详细版本 | 可以围绕商品描述的5点描述展开详细的说明，每一点“描述”都可以在商品说明中编写为一段话 |  |
| 后台关键词 | 专门用于亚马逊搜索 | 形容词+核心关键词+for/with+功能词 |  |
| 商品主图 | 展示在搜索结果页的图片 | 必须是白底图，像素至少1600+ |  |
| 商品附加图 | 除了商品主图以外的图片，一般用来呈现使用场景、尺寸说明、材质细节 |  |  |

## 标题的注意事项：

- 字符200以内，最好120-140，数量词用阿拉伯表示2．标题首位是品牌名，大小写要保持一致
- 不要放任何其他人品牌
- 不能全大写或者全小写，不能有任何特殊字符
- 标题简明扼要，不要太长，不建议使用太多形容词或添加过多细节
- 不能有公司，促销，物流，运费或其他任何与商品本身无关的信息，如freeshopping、hot sale
- 不要过多的“中性词”，比如漂亮、优秀
- 适用范围+产品参数准确的引导顾客正确的购买行为

## 5点描述的注意事项：

1. 是对商品材质/规格/功能/使用范围/使用方法等的简要描述。
2. 根据顾客对产品的关注顺序排列优先级，比如垃圾桶，顾客可能优先关注的是密闭性-材质-大小。
3. 句首，有总结性词语，最好是关键词3．每条字符限制500，但尽量简介明了
4. 不要放任何其他人品牌

## 产品说明的注意事项

1．涵盖主要特性、材质、颜色、数量、尺寸、产品的注意事项、使用方法、产品的包装内容等，切记不等于描述
2．尽量出现功能卖点关键词，
3．总分总结构（非必须，但最好），顾客阅读﹔亚马逊抓取都方便4．便于阅读
5．贴合实际，不夸大

## 后台关键词

1．核心关键词组（核心关键词+核心卖点词）放在最前面，其余的词需要根据具体产品的卖点重要程度来灵活调整先后次序。

2. 250字符，日语180西班牙语230，通常留出4-5个字符，以避免超出限制。






